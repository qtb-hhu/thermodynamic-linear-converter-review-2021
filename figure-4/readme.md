# Usage

1) `use_screen.jl` generates samples from _E. coli_ models
2) `post_process.jl` converts these samples into estimates of entropy production
3) `plot_res.jl` plots the results of (2)
