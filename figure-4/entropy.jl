function find_dgfs(modellist, system)
    dgf_dict = Dict{String, Any}()
    for mdl in modellist
        rxns = reactions(mdl)
        ex_rxn_ids = filter(looks_like_exchange_reaction, rxns)
        for ex_rxn_id in ex_rxn_ids
            if !haskey(dgf_dict, ex_rxn_id)
                ex_rxn_met = first(keys(mdl.reactions[ex_rxn_id].metabolites))[1:end-2] # exchange reactions only have one metabolite
                dgf_dict[ex_rxn_id] = try
                    standard_dg_prime(system, bigg(string(" = ", ex_rxn_met)), balance_warn=false)
                catch
                    (0 ± 10000.0)u"kJ/mol"
                end
            end  
        end
    end
    return dgf_dict
end

function remove_uncertain_dgs!(dfg_dict)
    for (k, v) in dfg_dict
        if Measurements.uncertainty(v) > 2000.0u"kJ/mol"
            delete!(dfg_dict, k)
        end
    end
end

function calculate_entropy_production(sol, ΔGs, temp)
    σ = 0.0 ± 0.0
    for (k, ΔfG) in ΔGs
        haskey(sol, k) && (σ += ustrip(ΔfG) * sol[k])
    end
    -σ/ustrip(uconvert(u"K", temp))
end
