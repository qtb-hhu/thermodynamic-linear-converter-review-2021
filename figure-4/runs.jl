runs = [(model_name = "EcoliK12_iML1515.json",
carbon_source = "EX_glc__D_e",
carbon_source_lb = -10.0,
oxygen_lb = 0.0),

(model_name = "EcoliK12_iML1515.json",
carbon_source = "EX_glc__D_e",
carbon_source_lb = -10.0,
oxygen_lb = -1000.0),

(model_name = "EcoliK12_iML1515.json",
carbon_source = "EX_xyl__D_e",
carbon_source_lb = -10.0,
oxygen_lb = -1000.0),

(model_name = "EcoliK12_iML1515.json",
carbon_source = "EX_xyl__D_e",
carbon_source_lb = -10.0,
oxygen_lb = 0.0),]
