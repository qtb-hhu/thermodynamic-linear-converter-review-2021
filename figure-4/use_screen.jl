using COBREXA
using Gurobi
using Serialization
using Distributed
include("runs.jl")

addprocs(55)
@everywhere using COBREXA, Gurobi

for run in runs
    model_name = run.model_name
    carbon_source = run.carbon_source
    carbon_source_lb = run.carbon_source_lb
    oxygen_lb = run.oxygen_lb
    
    model = load_model(StandardModel, joinpath("modelfiles", model_name))

    fva_mins, fva_maxs = flux_variability_analysis_dict(
        model,
        Gurobi.Optimizer;
        bounds = objective_bounds(0.99),
        modifications = [
            change_optimizer_attribute("OutputFlag", 0),
            change_constraint("EX_glc__D_e", 0.0, 0.0),
            change_constraint(carbon_source, carbon_source_lb, 1000.0),
            change_constraint("EX_o2_e", oxygen_lb, 1000.0),
        ],
        workers = workers(),
    )

    fva_bounds = [[(k, min(v[k]*0.99, v[k]*1.01), max(v[k]*0.99, v[k]*1.01),) for (k,v) in fva_mins];
                [(k, min(v[k]*0.99, v[k]*1.01), max(v[k]*0.99, v[k]*1.01),) for (k,v) in fva_maxs]]

    screen_out = @time screen(model;
        args = fva_bounds,  
        analysis = (m, k, vmin, vmax) ->
            parsimonious_flux_balance_analysis_dict(m,
                Gurobi.Optimizer;
                modifications= [change_optimizer_attribute("OutputFlag", 0),
                                change_constraint("EX_glc__D_e", 0.0, 0.0),
                                change_constraint(carbon_source, carbon_source_lb, 1000.0),
                                change_constraint("EX_o2_e", oxygen_lb, 1000.0),
                                change_constraint(k, vmin, vmax),],
            ),
            workers = workers(),
    )

    serialize(joinpath("results", string(model_name, "_", carbon_source, "_", carbon_source_lb, "_", "oxygen_", oxygen_lb, ".jls")), screen_out)
end
