using Measurements: length
using Serialization
using DataFrames
using Measurements
using Unitful
using Colors
using ColorSchemes
using CairoMakie
cd("entropy-production")

colors = ColorSchemes.Set2_4

σ_prod = deserialize(joinpath("results", "entropy_production.jls"))
σ_prod = σ_prod[ 0.0 .≤ σ_prod.σ .≤ 2.0, :] # remove outliers

# only plot the unique entropy production values for aesthetic purposes
models = ("E. coli (glc, aero)", "E. coli (glc, anaero)", "E. coli (xyl, aero)", "E. coli (xyl, anaero)")
σ_unique = DataFrame(model=String[], σ=Float64[])
for model_name in models
    vs = filter(x -> x.model == model_name, σ_prod).σ
    for v in unique(vs)
        push!(σ_unique, (model_name, v))
    end
end

titles = ["Glc, Aero",  "Glc, Anaero", "Xyl, Aero", "Xyl, Anaero"]

fig = Figure(font = assetpath("fonts", joinpath(pwd(),"font","latinmodern-math.otf")), fontsize=18);
ax1 = fig[1,1] = Axis(fig)
for (k, (condition, title)) in enumerate(zip(models, titles))
    ys = filter(x -> x.model == condition, σ_unique).σ
    xs = randn(length(ys)) .+ 6*k
    scatter!(ax1, xs, ys, color=colors[k], markersize=8)
end
hidexdecorations!(ax1)
hideydecorations!(ax1, ticklabels=false)
ylims!(ax1, 1.705, 1.75)
ax1.yticks = [1.72, 1.74]

ax2 = fig[2,1] = Axis(fig)
for (k, (condition, title)) in enumerate(zip(models, titles))
    ys = filter(x -> x.model == condition, σ_unique).σ
    xs = randn(length(ys)) .+ 6*k
    scatter!(ax2, xs, ys, color=colors[k],markersize=8)
end
hideydecorations!(ax2, ticklabels=false)
hidexdecorations!(ax2, ticklabels=false)
ylims!(ax2, 1.24, 1.39)     
ax2.xticks = ([6,12,18,24], titles)

rowsize!(fig.layout, 2, Relative(2/3))
rowgap!(fig.layout, 1, Relative(0.0))
ax2.topspinevisible=false
ax1.bottomspinevisible=false


Label(fig[:, 0], "Entropy production (kJ/K/molC)", rotation=pi/2)

fig

save("entropy_production_scatter.pdf", fig)
