using eQuilibrator
using COBREXA
using Unitful
using Measurements
using Serialization
using DataFrames
using Statistics
include("entropy.jl")

ΔfG_biomass = Dict("EcoliK12_iML1515.json" => 37.36 ± 6.20,)

pretty_names = Dict("EcoliK12_iML1515.json_EX_xyl__D_e_-10.0_oxygen_-1000.0.jls" => "E. coli (xyl, aero)",
                    "EcoliK12_iML1515.json_EX_glc__D_e_-10.0_oxygen_-1000.0.jls" => "E. coli (glc, aero)",
                    "EcoliK12_iML1515.json_EX_glc__D_e_-10.0_oxygen_0.0.jls" => "E. coli (glc, anaero)",
                    "EcoliK12_iML1515.json_EX_xyl__D_e_-10.0_oxygen_0.0.jls" => "E. coli (xyl, anaero)")

temp = 25u"°C"
i_strength = 250u"mM"
ph = 7.0
pmg = 3.0

system = eQuilibrator.System(pH = ph, pMg = pmg, temperature = temp, ionic_strength = i_strength)

σ_prod = DataFrame(model=String[], σ=Float64[])
for fname in readdir(joinpath("results"))
    # println(fname)
    model_name = split(fname, ".json")[1]*".json"
    model = load_model(StandardModel, joinpath("modelfiles", model_name))
    ΔGs = find_dgfs([model], system)
    sols = deserialize(joinpath("results", fname))

    σs = Dict{Int, Measurement{Float64}}()
    for (k, sol) in enumerate(sols)
        isnothing(sol) && continue
        σ = calculate_entropy_production(sol, ΔGs, temp) # units = J/gdW/h/K
        cflux = -atom_exchange(model, "BIOMASS_Ec_iML1515_core_75p37M")["C"] * sol["BIOMASS_Ec_iML1515_core_75p37M"] # C is produced by biomass
        σ += cflux * ΔfG_biomass[model_name] / ustrip(uconvert(u"K", temp)) # units = units = J/gdW/h/K 
        σs[k] = σ/cflux # units = J/K/mmolC == kJ/K/molC
    end
    
    vs = Float64[]
    for (k, v) in σs
        v = round(Measurements.value(v), digits=4)
        push!(σ_prod, (pretty_names[fname], v))        
    end
end

serialize(joinpath("results", "entropy_production.jls"), σ_prod)
