using Plots
using Colors
using ColorSchemes
using LaTeXStrings
pgfplotsx()
ssize = 14
default(ytickfontsize = ssize, xtickfontsize = ssize, yguidefontsize = ssize, xguidefontsize= ssize)

j(x, q) = @. -(x+q)/(q+1/x)

x = -1.0:0.001:0

function anno(startx, endx, xs, ys)
    startxind = argmin(abs.(xs .- startx))
    starty = ys[startxind+1]
    startx = xs[startxind+1]
    endxind = argmin(abs.(xs .- endx))
    endy = ys[endxind+1]
    m = (endy - starty)/(endx - startx)
    angle = rad2deg(atan(m))
    midx = (xs[startxind-1] + xs[endxind+1])/2
    midy = (ys[startxind-1] + ys[endxind+1])/2
    return angle, midx, midy, xs[1:startxind-1], ys[1:startxind-1], xs[endxind+1:end], ys[endxind+1:end] 
end

colors = ColorSchemes.Set2_5
xlims1 = (-1, 0)
ylims1 = (0, 1.05) 
begin
    p2 = plot(ylim=ylims1, xlim=xlims1, ylabel=L"\eta", xlabel="Force ratio (x)")
    for (q, col, xstart) in zip([0.999, 0.99, 0.9, 0.7, 0.5], 
                        colors,
                        [-0.95, -0.8, -0.6, -0.5, -0.4])
        ys = j(x, q)    
        if q == 0.999
            xadd = 0.125
        elseif q == 0.99
            xadd = 0.11
        else
            xadd = 0.1
        end
        angle, startx, starty, xs1, ys1, xs2, ys2 = anno(xstart, xstart+xadd, x, ys)
        plot!(p2, xs1, ys1, color=col, linewidth=2)
        plot!(p2, xs2, ys2, color=col, linewidth=2)
        if q==0.999
            adda = -15
        elseif q == 0.99
            adda = -10
        else
            adda = 0
        end
        plot!(p2, annotation = (startx, starty, Plots.text("q=$q", rotation=angle-adda)))
    end
    plot!(p2, legend=false, grid=false,framestyle=:box)
    hline!([ylims()[1]], color=:black)
    hline!([ylims()[2]], color=:black)
    vline!([xlims()[1]], color=:black)
    vline!([xlims()[2]], color=:black)
end
p2

savefig(p2, "efficiency.pdf")
