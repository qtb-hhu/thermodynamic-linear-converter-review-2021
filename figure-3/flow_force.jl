using Plots
using Colors
using ColorSchemes
using LaTeXStrings
pgfplotsx()
ssize = 14
default(ytickfontsize = ssize, xtickfontsize = ssize, yguidefontsize = ssize, xguidefontsize= ssize)

j(x, q) = @. (q + x)/(q * x + 1.0)

x = -1.0:0.001:0

function anno(startx, endx, xs, ys)
    startxind = argmin(abs.(xs .- startx))
    starty = ys[startxind+1]
    startx = xs[startxind+1]
    endxind = argmin(abs.(xs .- endx))
    endy = ys[endxind+1]
    m = (endy - starty)/(endx - startx)
    angle = rad2deg(atan(m))
    midx = (xs[startxind-1] + xs[endxind+1])/2
    midy = (ys[startxind-1] + ys[endxind+1])/2
    return angle, midx, midy, xs[1:startxind-1], ys[1:startxind-1], xs[endxind+1:end], ys[endxind+1:end] 
end

colors = ColorSchemes.Set2_5
begin
    p1 = plot(ylim=(0.0, 1.05), xlim=(-1, 0.0), ylabel="Flow ratio (j)", xlabel="Force ratio (x)")
    for (q, col, xstart) in zip([0.999, 0.99, 0.9, 0.7, 0.5], 
                        colors,
                        [-0.95, -0.9, -0.7, -0.525, -0.425])
        ys = j(x, q)    
        if q == 0.999
            xadd = 0.15
        elseif q == 0.99
            xadd = 0.125
        else
            xadd = 0.1
        end
        angle, startx, starty, xs1, ys1, xs2, ys2 = anno(xstart, xstart+xadd, x, ys)
        plot!(p1, xs1, ys1, color=col, linewidth=2)
        plot!(p1, xs2, ys2, color=col, linewidth=2)
        if q < 0.91
            adda = 10
        else
            adda = 10
        end
        plot!(p1, annotation = (startx, starty, Plots.text("q=$q", rotation=angle-adda)))
    end
    a = 1.0
    addjst = 0.0013 # arrow vertical line intercept pretty

    h = 0.12 # height of arrow
    from_x = -1.0 
    to_x = -0.5
    plot!(p1, [from_x, to_x], [h, h], arrow=true, color=:black, linewidth=1,alpha=a)
    plot!(p1, [to_x - addjst, to_x - addjst], [0, h], color=:black, linewidth=1,alpha=a)

    h = 0.09 # height of arrow
    from_x = -1.0 
    to_x = -0.7 
    plot!(p1, [from_x, to_x], [h, h], arrow=true, color=:black, linewidth=1,alpha=a)
    plot!(p1, [to_x - addjst, to_x - addjst], [0, h], color=:black, linewidth=1,alpha=a)

    h = 0.06 # height of arrow
    from_x = -1.0 
    to_x = -0.9 
    plot!(p1, [from_x, to_x], [h, h], arrow=true, color=:black, linewidth=1,alpha=a)
    plot!(p1, [to_x - addjst, to_x - addjst], [0, h], color=:black, linewidth=1,alpha=a)

    h = 0.03 # height of arrow
    from_x = -1.0 
    to_x = -0.99 
    plot!(p1, [from_x, to_x], [h, h], arrow=true, color=:black, linewidth=1,alpha=a)
    plot!(p1, [to_x - addjst, to_x - addjst], [0, h], color=:black, linewidth=1,alpha=a)

    plot!(p1; annotation = (-0.75, 0.16, Plots.text("Maintenance")))
    
    plot!(p1, legend=false, grid=false,framestyle=:box) 
    hline!([ylims()[1]], color=:black)
    hline!([ylims()[2]], color=:black)
    vline!([xlims()[1]], color=:black)
    vline!([xlims()[2]], color=:black)   
end

p1

savefig(p1, "flow_force.pdf")
